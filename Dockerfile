FROM node:latest
RUN apt-get update && apt-get install inotify-tools --assume-yes
ENV PATH=$PATH:./node_modules/.bin
WORKDIR /app
ENV NODE_OPTIONS="--experimental-json-modules"
