# Ideas
- Can my browser run this kata? - run the kata and show the result to see if the kata can be completed in the current browser
- Table overview of all katas, showing:
  - if they work in this browser
  - what have I finished already
  - what katas are missing
  - "How much of JS do I 'know'?"
- Group by kata-group (like "Array API", etc.)

- [ ] simplify the markdown parsing of katas, e.g. just parse H1+first-paragraph and the kata body (from H2 onwards) as markdown, not the complicated way its done now
- [ ] move the functions, which are spread across so many files into one file "load-kata", "load-kata-markdown" ... and their names mean almost nothing :(

# One page per kata (for explicit pages, SEO, insights on one page and link targets)

- [x] open kata's tddbin page
- [x] fix the H2 anchors on the overview pages
- [x] improve the katas content (the describe+it in the katas), which land on each kata's site
- [x] show the right metadata on the kata sites (links, related katas, level, ...)
- [x] fix the `twitter:title`, its currently `undefined`
- [x] move the parseint link into the metadata, not hardcoded in the template
- [ ] take it live (using codeberg pages?)
- [x] generate but dont link the es6/es2020 pages twice

- [x] make `/katas` the homepage, use the same canonical (for now)
- [x] generate all kata pages
  - [x] provide the md content via the KataMarkdownFile object
  - [x] a KataSite should require the `Kata`, `KataMarkdownFile` and maybe the `KataBundle` object
  - [x] ~~generate~~ provide all data for the md (files/string) for the kata pages
    - [x] provide a local script to convert test into md file
    - [x] generate the headers for the MD file, the H1 and the tags and attributes above it
    - [x] remove requirement for strange md-file prefix (which used to be useful for the blog, iirc)
    - [x] generate the md files for any number of tests (so we can later just loop over it when processing the meta files)
  - [x] provide all the tests+meta data to render the kata page
    - [x] get all meta files
      - [x] provide the locations of all meta files to be found under a certain root-URL
      - [x] download the all into a local cache (so we dont have to redo it every time), invalidate cache just by removing it
    - [x] provide a JS interface to the metadata
    - [x] loop over all metadata objects
    - [x] provide the md (string) from the tests+metadata
    - [x] generate the site (HTML) for per kata
    - [x] write it into the _output directory
    - [x] make this process run in the index.js when the server runs and update when data changes
  - [x] make backticks in kata titles render into the md file (some bash magic prevents this currently :( )
  - [x] provide the H1 for a md file, use the `groupName`+`name` of the kata
- [x] add types, strict and relentless (I know that would mean use rescript ... <excuse1>, <excuse2>, ...)
