dateCreated: 2021-12-26T13:56:42.025Z  

# Global Object API: `parseInt()` 
`parseInt()` parses a string and returns an integer.

## Summary
* `parseInt()` parses a string and returns an integer.
* it is a global function

## WHEN given a numeric string
* THEN converts it to an integer
* AND a radix=16 THEN converts to an integer applying the radix (like hexadecimal)
* AND a radix=9 THEN converts to an integer applying the radix
* AND a radix=0 THEN converts to an integer assuming radix=10
* AND the string starts with `0x` THEN converts to an integer assuming radix=16
* AND the string starts with `0X` THEN converts to an integer assuming radix=16
## WHEN given a NOT-numeric string (it will be tried to be converted to a string)
* a string starting with numbers THEN converts containing the leading numbers only
* a string starting with `-F` (a hexadecimal number) AND radix=16 THEN returns -15
* a string, a word made of letters only THEN returns `NaN` (not a number)
* an empty object literal THEN returns `NaN`
* an empty array THEN returns `NaN`
* an array with `[123,456]` THEN converts it to a string and then to an integer

