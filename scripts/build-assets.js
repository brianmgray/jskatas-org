#!/usr/bin/env zx

import {$, cd, fs, chalk} from 'zx';

// the former npm scripts
// "build:images:jpg": "cd content/blog; find . -name '*.jpg' -exec cp '{}' ../../_output/blog/'{}' \\;",
// "build:images:png": "cd content/blog; find . -name '*.png' -exec cp '{}' ../../_output/blog/'{}' \\;",
// "build:images:gif": "cd content/blog; find . -name '*.gif' -exec cp '{}' ../../_output/blog/'{}' \\;",
// "build:images:webp": "cd content/blog; find . -name '*.webp' -exec cp '{}' ../../_output/blog/'{}' \\;",

const buildImages = async () => {
  const imageExtensions = ['jpg', 'png', 'gif', 'webp'];
  cd('content/blog');
  await Promise.all(
    imageExtensions.map(ext => $`find . -name '*.${ext}' -exec cp '{}' ../../_output/blog/'{}' \\;`)
  );
  cd('../..');
};

// "build:assets": "cp .domains _output && npm run build:css && npm run build:favicon && npm run build:images:png && npm run build:images:jpg && npm run build:images:gif && npm run build:images:webp && cp templates/google22370c00d132ce23.html _output",
// "build:css": "cp templates/*.css _output/ && cp templates/blog/*.css _output/blog/ && cp templates/about/*.css _output/about/ && cp templates/katas/*.css _output/katas/",
// "build:favicon": "cp templates/*.ico _output/",

const buildAssets = async () => {
  return Promise.all([
    $`cp .domains _output`,
    $`cp templates/google22370c00d132ce23.html _output`,
    // css
    $`cp templates/*.css _output/ && cp templates/blog/*.css _output/blog/ && cp templates/about/*.css _output/about/ && cp templates/katas/*.css _output/katas/`,
    // favicon
    $`cp templates/*.ico _output/`,
    // components
    $`cp -R templates/components _output/`,
  ]);
};

await buildImages();
await buildAssets();