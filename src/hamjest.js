// This file is only here, until the PR https://github.com/rluba/hamjest/pull/50 is merged in hamjest
import * as hamjest from 'hamjest';

export const {
  assertThat, hasProperties, throws, instanceOf,
  contains, containsString,
  endsWith, startsWith,
  not, hasItem, hasProperty,
  Matcher
  // @ts-ignore
} = hamjest.default;