// @ts-nocheck
import {BLOG_POSTS_DIRECTORY} from "../config.js";
import {loadManyBlogPostSourceFiles} from "./load-blog-post-source-file.js";
import {sortByDateCreatedDescending} from "./sort-blog-post.js";
import {loadManyBlogPosts} from "./load-blog-post.js";
import {renderTemplate} from "../_deps/render-template.js";
import {writeOutputFile} from "../_deps/fs.js";

const isNotDraft = post => post.isDraft === false;
const loadPosts = async sourceFiles => {
  const posts = (await loadManyBlogPosts()(sourceFiles)).sort(sortByDateCreatedDescending);
  posts.excludingDrafts = () => posts.filter(isNotDraft);
  return posts;
}

const generateBlogPost = async (posts, post, pageParams) => {
  const renderedFile = renderTemplate('blog/post.html', {...pageParams, post, posts, pageTitlePrefix: post.headline});
  await writeOutputFile(`${post.url}/index.html`, renderedFile)
}

const generateOverviewPage = async (posts, pageParams) => {
  const renderedFile = renderTemplate('blog/overview.html', {...pageParams, posts, pageTitlePrefix: 'Blog'});
  await writeOutputFile('blog/index.html', renderedFile);
};

export const generateBlogPosts = async ({ pageParams }) => {
  const sourceFiles = await loadManyBlogPostSourceFiles()(BLOG_POSTS_DIRECTORY);
  const posts = await loadPosts(sourceFiles);
  return Promise.all([
    ...posts.map((post) => generateBlogPost(posts, post, pageParams)),
    generateOverviewPage(posts.excludingDrafts(), pageParams)
  ]);
}
