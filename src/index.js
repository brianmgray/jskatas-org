// @ts-nocheck TODO make this type checked, but for getting 0 errors for now, some files dont get checked yet. Improve it!
import * as path from 'path';
import * as marked from 'marked';
import * as fs from 'fs';

import {CONTENT_DIRECTORY} from './config.js';
import {renderTemplate} from './_deps/render-template.js';
import {generateKataSites} from "./generate-kata-sites/index.js";
import {generateBlogPosts} from "./generate-blog-sites/index.js";
import {writeOutputFile} from "./_deps/fs.js";

const navigationItems = [
  {path: '/katas', name: 'Katas', pagename: 'kata'},
  {path: '/blog', name: 'Blog', pagename: 'blog'},
  {path: '/about', name: 'About', pagename: 'about'},
];
const defaultRenderParams = {
  navigationItems,
  site: {
    title: 'JSKatas.org',
    subtitle: 'Continuously Learn JavaScript. Your Way.',
    domain: 'jskatas.org',
  },
};

const aboutIndexPage = async () => {
  const content = marked.marked(await fs.promises.readFile(path.join(CONTENT_DIRECTORY, 'about/index.md'), 'utf8'));
  const renderedFile = renderTemplate('about/index.html', {...defaultRenderParams, content, pageTitlePrefix: 'About'});
  await writeOutputFile('about/index.html', renderedFile);
};
const generateAboutPages = async () => {
  await aboutIndexPage();
}

const runAndTimeIt = async (label, fn) => {
  const paddedLabel = (label + new Array(20).fill(' ').join('')).substr(0, 25);
  console.time(paddedLabel);
  try {
    await fn();
  } catch(e) {
    console.error(e);
    process.exit(1); 
  }
  console.timeEnd(paddedLabel);
}

(async() => {
  console.time('Overall');
  console.log('Preparing data\n========');

  console.log('\nBuilding pages\n========');

  await runAndTimeIt('About pages', () => generateAboutPages());

  await runAndTimeIt(`Kata pages (???)`, () => generateKataSites({ pageParams: defaultRenderParams }));
  await runAndTimeIt(`Blog pages (???)`, () => generateBlogPosts({ pageParams: defaultRenderParams }));

  console.log('-----');
  console.timeEnd('Overall');
  console.log('-----');
})();
