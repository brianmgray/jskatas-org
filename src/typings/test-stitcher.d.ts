declare module 'test-stitcher' {

  type Test = { name: string }
  type Suite = { name: string, tests: Test[], suites: Suite[] }

  function extractTextFromFile(filename: string): Promise<{ suites: Suite[] }>
}