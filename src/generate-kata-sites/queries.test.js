import {describe, it} from '../test.js';
import {assertThat, contains, hasProperties} from '../hamjest.js';
import {forTestingOnly, KataBundle} from './KataBundle.js';
import {forTestingOnly as forTestingKataOnly, Kata} from './Kata.js';
import {findKatasWithSameGroupName, findRequiredKnowledgeKatas} from "./queries.js";

/**
 * @typedef {import('./KataBundle').RawKataBundleFromMetadata} RawKataBundleFromMetadata
 * @typedef {import('./Kata').RawKataFromMetadata} RawKataFromMetadata
 */

const { oneRawKataBundleObject } = forTestingOnly.fixtures;
const { oneRawKataObject } = forTestingKataOnly.fixtures;

/** @type {function(RawKataFromMetadata): Kata} */
const oneKata = (raw) => Kata.fromRawMetadataObject(raw, {bundleId: ''});
/** @type {function(RawKataBundleFromMetadata): KataBundle} */
const oneBundle = (raw) => KataBundle.fromRawMetadataObject(raw, {bundleId: ''});

describe('Find katas with same group name, given one kata', () => {
  it('WHEN one bundle given AND options.including=false THEN find all its katas but the one searched for', () => {
    const kataItems = [
      oneRawKataObject({name: 'kata1', groupName: 'one'}),
      oneRawKataObject({name: 'kata2', groupName: 'one'}),
      oneRawKataObject({name: 'kata3', groupName: 'one'}),
    ];
    const bundles = [oneBundle(oneRawKataBundleObject({ items: kataItems }))];
    const kata = oneKata(kataItems[0]);
    const relatedKatas = findKatasWithSameGroupName(bundles, kata, { including: false });
    assertThat(relatedKatas, contains(
      hasProperties({ name: 'kata2' }),
      hasProperties({ name: 'kata3' }),
    ));
  });
  describe('WHEN the kata we are searching for is the only one in its group', () => {
    it('AND it shall NOT be included in the result THEN return empty list', () => {
      const kataItems = [
        oneRawKataObject({name: 'kata1', groupName: 'one'}),
        oneRawKataObject({name: 'kata2', groupName: 'two'}),
        oneRawKataObject({name: 'kata3', groupName: 'three'}),
      ];
      const bundles = [oneBundle(oneRawKataBundleObject({ items: kataItems }))];
      const kata = oneKata(oneRawKataObject(kataItems[2]));
      assertThat(findKatasWithSameGroupName(bundles, kata, { including: false }), []);
    });
    it('AND it shall be included in the result THEN return empty list', () => {
      const kataItems = [
        oneRawKataObject({name: 'kata1', groupName: 'one'}),
        oneRawKataObject({name: 'kata2', groupName: 'two'}),
        oneRawKataObject({name: 'kata3', groupName: 'three'}),
      ];
      const bundles = [oneBundle(oneRawKataBundleObject({ items: kataItems }))];
      const kata = oneKata(oneRawKataObject(kataItems[2]));
      assertThat(findKatasWithSameGroupName(bundles, kata, { including: true }), contains(
        hasProperties({ name: 'kata3' })
      ));
    });
  });
  it('WHEN there are katas in multiple bundles THEN return all with the same groupName, including the one searched for (which is the default option)', () => {
    const kataItems1 = [
      oneRawKataObject({name: 'kata1.1', groupName: 'one'}),
      oneRawKataObject({name: 'kata1.2', groupName: 'two'}),
      oneRawKataObject({name: 'kata1.3', groupName: 'three'}),
      oneRawKataObject({name: 'kata1.4', groupName: 'three'}), // we will search for this one
    ];
    const kataItems2 = [
      oneRawKataObject({name: 'kata2.1', groupName: 'one'}),
      oneRawKataObject({name: 'kata2.2', groupName: 'two'}),
      oneRawKataObject({name: 'kata2.3', groupName: 'three'}),
    ];
    const bundles = [
      oneBundle(oneRawKataBundleObject({ items: kataItems1 })),
      oneBundle(oneRawKataBundleObject({ items: kataItems2 })),
    ];
    const kata = oneKata(oneRawKataObject(kataItems1[3]));
    assertThat(findKatasWithSameGroupName(bundles, kata), contains(
      hasProperties({ name: 'kata1.3' }),
      hasProperties({ name: 'kata1.4' }),
      hasProperties({ name: 'kata2.3' }),
    ));
  });
});

describe('Find required-knowledge katas, for a given kata', () => {
  it('WHEN a kata has no required-knowledge katas THEN the list is empty', () => {
    const kata = Kata.fromRawMetadataObject(oneRawKataObject({requiresKnowledgeFrom: []}), {bundleId: ''});
    assertThat(findRequiredKnowledgeKatas([], kata), []);
  });
  it('WHEN kataA is required by kataB THEN return an array with kataA', () => {
    const bundleId = 'the bundle ID';
    const rawKataA = oneRawKataObject({name: 'kataA', id: 1});
    const rawKataB = oneRawKataObject({name: 'kataB', id: 2, requiresKnowledgeFrom: [{bundle: bundleId, id: 1}]});
    const bundle = KataBundle.fromRawMetadataObject(oneRawKataBundleObject({items: [rawKataA, rawKataB]}), {bundleId});
    const kataB = Kata.fromRawMetadataObject(rawKataB, {bundleId});
    assertThat(
      findRequiredKnowledgeKatas([bundle], kataB),
      contains(hasProperties({name: 'kataA'}))
    );
  });
  it('WHEN multiple katas are required-knowledge THEN provide them', () => {
    const bundle1Id = 'the bundle ONE ID';
    const bundle2Id = 'the bundle TWO ID';
    const rawKataA = oneRawKataObject({name: 'kata1.A', id: 1});
    const rawKataB = oneRawKataObject({name: 'kata1.B', id: 2});
    const rawKataC = oneRawKataObject({name: 'kata2.C', id: 1});
    const rawKataD = oneRawKataObject({name: 'kata2.B', id: 2, requiresKnowledgeFrom: [
      {bundle: bundle1Id, id: 1}, {bundle: bundle1Id, id: 2}, {bundle: bundle2Id, id: 1}
    ]});
    const bundle1 = KataBundle.fromRawMetadataObject(oneRawKataBundleObject({items: [rawKataA, rawKataB]}), {bundleId: bundle1Id});
    const bundle2 = KataBundle.fromRawMetadataObject(oneRawKataBundleObject({items: [rawKataC, rawKataD]}), {bundleId: bundle2Id});
    const kataD = Kata.fromRawMetadataObject(rawKataD, {bundleId: bundle2Id});
    assertThat(
      findRequiredKnowledgeKatas([bundle1, bundle2], kataD),
      contains(hasProperties({name: 'kata1.A'}), hasProperties({name: 'kata1.B'}), hasProperties({name: 'kata2.C'})),
    );
  });
});
