/**
 * @typedef {import('./KataBundle').KataBundle} KataBundle
 * @typedef {import('./Kata').Kata} Kata
 */

/**
 * @param bundles {KataBundle[]}
 * @param kata {Kata}
 * @param options {{ including: boolean }}
 * @return kata {Kata[]}
 */
export const findKatasWithSameGroupName = (bundles, kata, options = { including: true }) => {
  const groupNameToFind = kata.groupName;
  /** @type {function(Kata): boolean} */
  const isSameGroup = (kata) => kata.groupName === groupNameToFind;
  const katas = bundles
    .map((bundle) => { return bundle.katas.filter(isSameGroup); })
    .flat();
  return options.including ? katas : katas.filter((k) => { return k.isEqual(kata) === false; });
};

/**
 * @param bundles {KataBundle[]}
 * @param kata {Kata}
 * @return kata {Kata[]}
 */
export const findRequiredKnowledgeKatas = (bundles, kata) => {
  const requiredIds = kata.requiresKnowledgeFrom;
  /** @type {function(Kata): boolean} */
  const isInRequiredKnowledge = (k) => {
// TODO this MUST go into the Kata, the comparison if a kata is a requiredKnowledge
    const kataIds = requiredIds.filter(id => id.id===k.id.id && id.bundle===k.id.bundle);
    return kataIds.length > 0;
  };
  if (bundles.length) {
    const katas = bundles.map(b => b.katas).flat();
    return katas.filter(isInRequiredKnowledge);
  }
  return [];
};
