import {Kata, RawKataFromMetadata} from "./Kata";

type RawKataBundleFromMetadata = {
  name: string;
  nameSlug: Slug;
  items: RawKataFromMetadata[];
};

type KataBundleId = string;
type KataBundleConfig = {
  bundleId: KataBundleId;
  rootUrl?: string;
}
export class KataBundle {
  id: KataBundleId;
  name: string;
  nameSlug: Slug;
  katas: Kata[];
  kataGroups: Kata[][];
  rootUrl: string;
  static fromRawMetadataObject: (obj: RawKataBundleFromMetadata, config: KataBundleConfig) => KataBundle;
}

type ForTestingOnly = {
  fixtures: {
    oneRawKataBundleObject: (partial?: Partial<RawKataBundleFromMetadata>) => RawKataBundleFromMetadata;
  }
};
export const forTestingOnly: ForTestingOnly;
