import assert from 'assert/strict';

import {describe, it} from '../test.js';
import {assertThat, contains, containsString, hasProperties, throws} from '../hamjest.js';
import {forTestingOnly, Kata} from './Kata.js';
// @ts-ignore no idea how to make tsc pass for the following line, :shrug:
import testKataBundle from '../../test-content/katas/es10/language/__all__.json' assert { type: "json" };

const {oneRawKataObject, oneRawKataLink} = forTestingOnly.fixtures;

/**
 * @typedef {import('./KataBundle').RawKataBundleFromMetadata} RawKataBundleFromMetadata
 * @typedef {import('./Kata').RawKataFromMetadata} RawKataFromMetadata
 */

/** @type {function(RawKataFromMetadata): Kata} */
const oneKata = (raw) => Kata.fromRawMetadataObject(raw, {bundleId: ''});

const notAString = 0;
const notAnArray = '';
const notANumber = '';

describe('The Kata', () => {
  it('WHEN `name` is missing THEN throw', () => {
    const kataItem = oneRawKataObject();
    // @ts-ignore
    delete kataItem.name;
    assertThat(() => { oneKata(kataItem); },
      throws(hasProperties({message: 'Missing props in kata object: name'}))
    );
  });
  it('WHEN all props are missing THEN throw and list all props', () => {
    let errorMessage;
    try {
      // @ts-ignore
      oneKata({});
    } catch (error) {
      errorMessage = /** @type {Error} */(error).message;
    }
    assertThat(errorMessage, containsString('name'));
    assertThat(errorMessage, containsString('description'));
    assertThat(errorMessage, containsString('path'));
    assertThat(errorMessage, containsString('level'));
    assertThat(errorMessage, containsString('requiresKnowledgeFrom'));
    assertThat(errorMessage, containsString('groupName'));
    assertThat(errorMessage, containsString('groupNameSlug'));
    assertThat(errorMessage, containsString('id'));
  });
  it('WHEN props have the wrong type THEN throw and tell so', () => {
    let errorMessage;
    const wrongTypedRawKata = {
      name: notAString,
      description: notAString,
      path: notAString,
      level: notAString,
      requiresKnowledgeFrom: notAnArray,
      groupName: notAString,
      groupNameSlug: notAString,
      id: notANumber,
    }
    try {
      // @ts-ignore
      oneKata(wrongTypedRawKata);
    } catch (error) {
      errorMessage = /** @type {Error} */(error).message;
    }
    assertThat(errorMessage, containsString('name (string)'));
    assertThat(errorMessage, containsString('description (string)'));
    assertThat(errorMessage, containsString('path (string)'));
    assertThat(errorMessage, containsString('level (string)'));
    assertThat(errorMessage, containsString('requiresKnowledgeFrom (array)'));
    assertThat(errorMessage, containsString('groupName (string)'));
    assertThat(errorMessage, containsString('groupNameSlug (string)'));
    assertThat(errorMessage, containsString('id (number)'));
  });

  describe('construction', () => {
    it('WHEN calling the constructor THEN throw', () => {
      // must be initialized via factory method
    });
  });

  describe('the id', () => {
    it('WHEN given a kataBundleId THEN build the complete kata-id ', () => {
      const kata = Kata.fromRawMetadataObject(oneRawKataObject({id: 42}), { bundleId: 'bundle/id' })
      assertThat(kata.id, {bundle: 'bundle/id', id: 42});
    });
  });

  describe('the publish-date and isPublished flag', () => {
    it('WHEN the date is not parseable THEN throw', () => {
      const rawKata = oneRawKataObject({ publishDateRfc822: 'invalid date' });
      assert.throws(() => oneKata(rawKata), {
        name: 'Error',
        message: 'Invalid date for field: "publishDateRfc822".'
      });
    });
    it('WHEN no publish-date is given THEN isPublished=false AND `readableCreatedAt` is empty', () => {
      const rawKata = oneRawKataObject();
      delete rawKata.publishDateRfc822;
      const kata = oneKata(rawKata);
      assertThat(kata, hasProperties({ isPublished: false, readableCreatedAt: '' }));
    });
    describe('WHEN publish-date is given', () => {
      const rawKata = oneRawKataObject({ publishDateRfc822: '1 Jan 2000' });
      it('THEN isPublished=true', () => {
        const kata = oneKata(rawKata);
        assertThat(kata, hasProperties({ isPublished: true }));
      });
      it('AND readableCreatedAt is a readable date', () => {
        const kata = oneKata(rawKata);
        assertThat(kata, hasProperties({ readableCreatedAt: '1 January 2000' }));
      });
    });
  });

  describe('the `siteUrl`', () => {
    it('WHEN a rootUrl is missing THEN the `path` is the `siteUrl`', () => {
      const rawKata = oneRawKataObject({ path: '/the-kata/path' });
      const kata = oneKata(rawKata);
      assertThat(kata, hasProperties({ siteUrl: '/the-kata/path' }));
    });
    it('WHEN a rootUrl is given THEN the `path` is added and it makes the `siteUrl`', () => {
      const rawKata = oneRawKataObject({ path: '/the-kata/path' });
      const kata = Kata.fromRawMetadataObject(rawKata, { rootUrl: '/root-url/', bundleId: '' });
      assertThat(kata, hasProperties({ siteUrl: '/root-url/the-kata/path' }));
    });
  });

  describe('the `links`', () => {
    it('WHEN `links` are not given THEN return an empty list', () => {
      const rawKata = oneRawKataObject();
      delete rawKata.links;
      const kata = Kata.fromRawMetadataObject(rawKata, { rootUrl: '/root-url/', bundleId: '' });
      assertThat(kata.links, []);
    });
    it('WHEN links are NOT an array THEN throw the error', () => {
      // @ts-ignore
      const rawKata = oneRawKataObject({ links: '' });
      assert.throws(() => oneKata(rawKata), {
        name: 'TypeError',
        message: 'Wrong types in kata object, should be like so: links (array)'
      });
    });
    it('WHEN a link is missing one of `url` or `comment` THEN throw an error', () => {
      const validKataLink = oneRawKataLink();
      const rawKata = oneRawKataObject({ links: [validKataLink, {}] });
      assert.throws(() => oneKata(rawKata), {
        name: 'TypeError',
        message: 'Missing props in KataLink object: url, comment'
      });
    });
    it('WHEN a link has a wrong type in any of its props THEN throw an error', () => {
      const wronglyTypedProps = { url: notAString, comment: notAString, tags: notAnArray };
      const validKataLink = oneRawKataLink();
      const rawKata = oneRawKataObject({ links: [validKataLink, wronglyTypedProps] });
      assert.throws(() => oneKata(rawKata), {
        name: 'TypeError',
        message: 'Wrong types in KataLink object, should be like so: url (string), comment (string), tags (array)'
      });
    });
    it('WHEN valid `links` are given THEN provide them', () => {
      const links = [oneRawKataLink({ url: 'jskatas.org', comment: 'Continuously learn JS', tags: ['javascript', 'learning'] })];
      const kata = Kata.fromRawMetadataObject(oneRawKataObject({links}), { rootUrl: '/root-url/', bundleId: '' });
      assertThat(kata.links, contains({
        url: 'jskatas.org', comment: 'Continuously learn JS', tags: ['javascript', 'learning']
      }));
    });
  });

  describe('the `requiresKnowledgeFrom`', () => {
    const validKataRef = {bundle: '', id: 1};
    it('WHEN a prop `bundle` or `id` is missing THEN throw an error', () => {
      const invalidKataRef = {};
      const rawKata = oneRawKataObject({requiresKnowledgeFrom: [validKataRef, invalidKataRef]});
      assert.throws(() => oneKata(rawKata), {
        name: 'TypeError',
        message: 'Missing props in KataId object: bundle, id'
      });
    });
    it('WHEN one item has a wrong type in any of its props THEN throw an error', () => {
      const wronglyTypedProps = { bundle: notAString, id: notANumber };
      const rawKata = oneRawKataObject({requiresKnowledgeFrom: [validKataRef, wronglyTypedProps]});
      assert.throws(() => oneKata(rawKata), {
        name: 'TypeError',
        message: 'Wrong types in KataId object, should be like so: bundle (string), id (number)'
      });
    });
    it('WHEN valid `requiresKnowledgeFrom` are given THEN provide them', () => {
      const kata = oneKata(oneRawKataObject({requiresKnowledgeFrom: [{ bundle: 'bundle/id', id: 42 }]}));
      assertThat(kata.requiresKnowledgeFrom, contains({ bundle: 'bundle/id', id: 42 }));
    });
  });

  xit('all Katas from `test-content/katas/es10/language/__all__.json` can be converted to valid Kata instance', () => {
    const rawKatas = /** @type {RawKataFromMetadata[]} */(testKataBundle.items);
    assert.doesNotThrow(() => rawKatas
      .forEach(rawKata => oneKata(rawKata))
    );
  });
});
