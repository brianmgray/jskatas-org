import {Kata} from "./Kata.js";
import {checkObject} from "./_checkObject.js";

/**
 * @typedef {import('./KataBundle').RawKataBundleFromMetadata} RawKataBundleFromMetadata
 */

export class KataBundle {
  /**
   * @param obj {import('./KataBundle').RawKataBundleFromMetadata}
   * @param configParam {import('./KataBundle').KataBundleConfig}
   * @return {import('./KataBundle').KataBundle}
   */
  static fromRawMetadataObject(obj, configParam) {
    const config = {rootUrl: '', ...configParam};

    const checkResult = checkObject(obj, oneRawKataBundleObject());
    if (checkResult.isValid === false) {
      if (checkResult.missing.length > 0) {
        throw new Error('Missing props in KataBundle object: ' + checkResult.missing.join(', '));
      }
      if (checkResult.wrongTypes.length > 0) {
        throw new Error('Wrong types in KataBundle object, should be like so: ' + checkResult.wrongTypes.join(', '));
      }
    }
    const instance = /** @type {import('./KataBundle').KataBundle} */(new KataBundle());
    instance.name = String(obj.name);
    instance.nameSlug = String(obj.nameSlug);
    instance.katas = (obj.items ?? []).map((kata) => Kata.fromRawMetadataObject(kata, { rootUrl: config.rootUrl, bundleId: config.bundleId }));
    instance.rootUrl = String(config.rootUrl);
    return instance;
  }

  /**
   * @this {import('./KataBundle').KataBundle}
   */
  get kataGroups() {
    /** @type {PlainObject} */
    const groups = {};
    this.katas.forEach((kata) => {
      if (Array.isArray(groups?.[kata.groupName]) === false) {
        groups[kata.groupName] = [];
      }
      groups[kata.groupName].push(kata);
    });
    return Object.values(groups).sort((a, b) => a.length < b.length ? 1 : -1);
  }
}

/**
 * @param partial {Partial<RawKataBundleFromMetadata>}
 * @return {RawKataBundleFromMetadata}
 */
const oneRawKataBundleObject = (partial = {}) => {
  return {
    name: '',
    nameSlug: '',
    items: [],
    ...partial,
  };
};

/**
 * @type {import('./KataBundle').ForTestingOnly}
 */
export const forTestingOnly = {
  fixtures: { oneRawKataBundleObject },
};
