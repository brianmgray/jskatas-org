import * as path from 'path';
import {assertThat, startsWith, containsString} from '../hamjest.js';

import {TEST_CONTENT_DIRECTORY} from "../config.js";
import {describe, it} from '../test.js';
import {testFileToMarkdown} from './load-kata.js';

/**
 * @typedef {import('./load-kata').TestFileToMarkdownParams} TestFileToMarkdownParams
 */

const testFileName = (file = 'big-complex-kata.test.js') => {
  return path.join(TEST_CONTENT_DIRECTORY, file);
};
/**
 * @param {Partial<TestFileToMarkdownParams>?} params
 * @return {Promise<string>}
 */
const loadAndConvert = (params= {}) => {
  return testFileToMarkdown({ now: new Date() })({fileName: testFileName(), title: '', subtitle: '', ...params});
};
/**
 * @param {Partial<TestFileToMarkdownParams>?} params
 * @return {Promise<string>}
 */
const loadAndConvertMiniTestFile = (params= {}) => {
  const fileName = testFileName('mini-kata.test.js');
  return testFileToMarkdown({ now: new Date() })({fileName, title: '', subtitle: '', ...params});
};

describe('Load tests and convert to markdown', () => {
  describe('WHEN a test file was loaded and converted to a markdown string', () => {
    it('THEN the markdown starts with `dateCreated` at the top AND and empty line after it', async () => {
      const now = new Date(Date.UTC(2000, 1, 2, 3, 4));
      const markdown = await testFileToMarkdown({ now })({fileName: testFileName(), title: '', subtitle: ''});
      assertThat(markdown, startsWith('dateCreated: 2000-02-02T03:04:00.000Z  \n\n'))
    });

    it('THEN the function parameter `title` becomes the H1', async () => {
      const markdown = await loadAndConvert({title: 'The title'});
      assertThat(markdown, containsString('# The title\n'))
    });
    it('THEN the function parameter `subtitle` becomes the paragraph right after this H1', async () => {
      const markdown = await loadAndConvert({title: 'The title', subtitle: 'The subtitle is normally the katas description'});
      assertThat(markdown, containsString('# The title\n\nThe subtitle is normally the katas description'))
    });

    it('THEN the 1st `describe` becomes the first H2', async () => {
      const markdown = await loadAndConvert();
      assertThat(markdown, containsString('## The first describe\n'))
    });
    it('THEN the 1st `it` after `describe` is the 1st bullet point after an empty line after the H2', async () => {
      const markdown = await loadAndConvert();
      assertThat(markdown, containsString('## The first describe\n\n* tests #1.0\n'));
    });
    it('THEN the 2nd `it` after `describe` is the 2nd bullet point', async () => {
      const markdown = await loadAndConvert();
      assertThat(markdown, containsString('* tests #1.0\n* tests #1.1\n'));
    });

    describe('the H2s', () => {
      it('THEN the 2nd `describe` is the first H3', async () => {
        const markdown = await loadAndConvert();
        assertThat(markdown, containsString('\n### test group #1\n'));
      });
      it('THEN the first H3 is followed by a list of the tests', async () => {
        const markdown = await loadAndConvert();
        assertThat(markdown, containsString('### test group #1\n\n* test #2\n* test #3\n'));
      });

      it('THEN the next `describe` on this level renders an H3 AND an empty line above', async () => {
        const markdown = await loadAndConvert();
        assertThat(markdown, containsString('\n\n### test group #2'));
      });
      it('THEN the first H3 is followed by a list of the tests', async () => {
        const markdown = await loadAndConvert();
        assertThat(markdown, containsString('* test #8\n'));
        assertThat(markdown, containsString('* test #9\n'));
      });
    });

    describe('the H4 on the next level', () => {
      it('THEN the 3rd `describe` is the first H4', async () => {
        const markdown = await loadAndConvert();
        assertThat(markdown, containsString('#### test group #1.1'));
      });
      it('THEN the first H4 is followed by a list of the tests', async () => {
        const markdown = await loadAndConvert();
        assertThat(markdown, containsString('\n\n* test #4\n'));
        assertThat(markdown, containsString('* test #5\n'));
        assertThat(markdown, containsString('* test #6\n'));
        assertThat(markdown, containsString('* test #7\n'));
      });

      it('THEN every paragraph ends with an empty new line', async () => {
        const markdown = await loadAndConvert();
        assertThat(markdown, containsString('* test #3\n'));
        assertThat(markdown, containsString('* test #7\n'));
      });
    });

    describe('a file with three nested describes, before the first `it`', () => {
      const loadAndConvert = loadAndConvertMiniTestFile;
      it('THEN the 1st `describe` becomes the H2', async () => {
        const markdown = await loadAndConvert();
        assertThat(markdown, containsString('## top level describe\n\n'))
      });
      it('THEN the 2nd `describe` becomes the H3', async () => {
        const markdown = await loadAndConvert();
        assertThat(markdown, containsString('### 1st level describe'))
      });
    });
  });
});
