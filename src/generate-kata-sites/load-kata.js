import {extractTextFromFile} from 'test-stitcher';

/**
 * @typedef {import('test-stitcher').Test} Test
 * @typedef {import('test-stitcher').Suite} Suite
 */

/**
 * @param tests {Test[]}
 * @return {string[]}
 */
const testsToList = (tests) => {
  return tests.map(test => `* ${test.name}`);
};

/**
 * @param suite {Suite}
 * @param headingLevel {number}
 * @return {string[]}
 */
const suiteToParagraph = (suite, headingLevel) => {
  const headingPrefix = Array(headingLevel).fill('#').join('');
  return [
    `${headingPrefix} ${suite.name}\n`,
    ...testsToList(suite.tests),
    ``,
    ...suite.suites.flatMap(suite => suiteToParagraph(suite, headingLevel + 1)),
  ];
};

/**
 * @param {import("./load-kata").TestFileToMarkdownDeps} deps
 * @return {function({fileName: Filename, title: string, subtitle: string}): Promise<string>}
 */
export const testFileToMarkdown = ({ now} = { now: new Date() }) => async ({fileName, title, subtitle}) => {
  const text = await extractTextFromFile(fileName);
  const lines = [
    `dateCreated: ${new Date(now).toISOString()}  `,
    ``,
    `# ${title}\n`,
    `${subtitle}`,
    ``,
    ...text.suites.flatMap(suite => suiteToParagraph(suite, 2)),
  ];
  return lines.join('\n') + '\n';
};
